def editarProyecto(id)
  limpiar
  proyectos = $proyectos

  puts "Editar Proyecto"
  puts "(*) Presione enter para no hacer cambios"
  puts ""

  proyectos.each do |proyecto|
    proyecto.each do |p|
      if p['id'].to_i == id.to_i

        puts "Area: " + p['area']
        print "Nueva Area: "
        area = gets.chomp
        if area != ""
          p['area'] = area
        end

        puts ""

        puts "Proyecto: " + p['proyecto']
        print "Nuevo Proyecto: "
        proyecto = gets.chomp
        if proyecto != ""
          p['proyecto'] = proyecto
        end

        puts ""

        puts "Encargado: " + p['encargado']
        print "Nuevo Encargado: "
        encargado = gets.chomp
        if encargado != ""
          p['encargado'] = encargado
        end

        puts ""

        puts "Prioridad: " + p['prioridad']
        print "Nueva Prioridad: "
        prioridad = gets.chomp
        if prioridad != ""
          p['prioridad'] = prioridad
        end

        puts ""

        puts "Fecha Inicio: " + p['fecha_inicio']
        print "Nueva Fecha Inicio: "
        fecha_inicio = gets.chomp
        if fecha_inicio != ""
          p['fecha_inicio'] = fecha_inicio
        end

        puts ""

        puts "Fecha Fin: " + p['fecha_fin']
        print "Nueva Fecha Fin: "
        fecha_fin = gets.chomp
        if fecha_fin != ""
          p['fecha_fin'] = fecha_fin
        end

        puts ""

        puts "Objetivo: " + p['objetivo']
        print "Nuevo Objetivo: "
        objetivo = gets.chomp
        if objetivo != ""
          p['objetivo'] = objetivo
        end

        puts ""

        puts "Comentarios: " + p['comentarios']
        print "Nuevos Comentarios: "
        comentarios = gets.chomp
        if comentarios != ""
          p['comentarios'] = comentarios
        end

        break
      end
    end
  end

  detalleProyecto(id)

end

def eliminarProyecto(id)
  proyectos = $proyectos
  i = 0

  proyectos.each do |proyecto|
    proyecto.each do |p|
      if p['id'].to_i == id.to_i
        proyectos.delete_at(i)
        break
      end
    end
    i = i + 1
  end

  listarProyectos

end

def detalleProyecto(id)
  limpiar
  proyectos = $proyectos

  encontro = false
  proyect = Array.new

  proyectos.each do |proyecto|
    proyecto.each do |p|
      if p['id'].to_i == id.to_i
        puts "Area: " + p['area']
        puts "Proyecto: " + p['proyecto']
        puts "Encargado: " + p['encargado']
        puts "Prioridad: " + p['prioridad']
        puts "Fecha Inicio: " + p['fecha_inicio']
        puts "Fecha Fin: " + p['fecha_fin']
        puts "Objetivo: " + p['objetivo']
        puts "Comentarios: " + p['comentarios']
        encontro = true
        break
      end
    end
    if encontro
      break
    end
  end

  if encontro == false
    # puts "Opción invalida"
    listarProyectos
  else
    puts ""
    puts "(1) Editar"
    puts "(2) Eliminar"
    puts "(0) Salir"
    puts ""
    print "Seleccione una opción: "
    op = gets.chomp.to_i
  end

  if op == 1
    editarProyecto(id)
  elsif op == 2
    eliminarProyecto(id)
  else
    menuPrincipal
  end

end

def listarProyectos
  limpiar
  proyectos = $proyectos

  puts "Lista de Proyectos"

  puts "(0) Salir"
  puts ""

  proyectos.each do |proyecto|
    proyecto.each do |p|
      puts p['id'] + '- ' + p['proyecto']
    end
  end

  puts ""
  print "Seleccione proyecto: "
  p = gets.chomp.to_i

  if p == 0
    menuPrincipal
  else
    detalleProyecto(p)
  end

end

def registrarProyecto
  limpiar
  puts "Registrar Proyecto"

  print "Area\t\t: "
  area = gets.chomp

  print "Proyecto\t: "
  proyecto = gets.chomp

  print "Encargado\t: "
  encargado = gets.chomp

  print "Prioridad\t: "
  prioridad = gets.chomp

  print "Fecha Inicio\t: "
  fecha_inicio = gets.chomp

  print "Fecha Fin\t: "
  fecha_fin = gets.chomp

  print "Objetivo\t: "
  objetivo = gets.chomp

  print "Comentarios\t: "
  comentarios = gets.chomp

  proyectos = $proyectos

  if (proyectos.size == 0)
    id = "1"
  else
    id = (proyectos.last[0]['id'].to_i + 1).to_s
  end

  p = [
        'id' => id,
        'area' => area,
        'proyecto' => proyecto,
        'encargado' => encargado,
        'prioridad' => prioridad,
        'fecha_inicio' => fecha_inicio,
        'fecha_fin' => fecha_fin,
        'estado' => 'creado',
        'objetivo' => objetivo,
        'comentarios' => comentarios
       ]

  $proyectos.push p

  # puts $proyectos

  listarProyectos
end

def menuPrincipal
  limpiar
  puts "Seleccione una opción"
  puts "1- Registrar proyecto"
  puts "2- Listar proyectos"
  puts ""
  print "Opción: "

  opcion = gets.chomp.to_i

  case opcion
    when 1
      registrarProyecto
    when 2
      listarProyectos
    else
      puts "Opción Invalida"
  end
end

def limpiar
  system('cls')
end

$proyectos = [
    [
        'id' => '1',
        'area' => 'Producción',
        'proyecto' => 'Implementación de Terminales Lectores de Código de Barra',
        'encargado' => 'Barcoding - Daniel Rojas',
        'prioridad' => 'Alta',
        'fecha_inicio' => '2014-03-17',
        'fecha_fin' => '2014-04-30',
        'estado' => 'cancelado',
        'objetivo' => 'Automatizar el Control de la Producción mediante Terminales Lectores de Código de Barra.',
        'comentarios' => 'Se retomo el proyecto, y se finalizo sin problemas.'
    ],
    [
        'id' => '2',
        'area' => 'Logística',
        'proyecto' => 'Interfaz de Control de Productos Mínimos.',
        'encargado' => 'Daniel Rojas',
        'prioridad' => 'Media',
        'fecha_inicio' => '2014-02-24',
        'fecha_fin' => '2014-03-15',
        'estado' => 'terminado',
        'objetivo' => 'Crear una interfaz que facilite el control de Stocks mínimos el cual no está abarcado en el Sistema A.N.T.',
        'comentarios' => 'Se detuvo hasta finalizar el proyecto de Producción.'
    ]
]

menuPrincipal

# puts array[0]['nombre'].to_s

# puts Time.now